export enum ENetworkName {
  main = "main",
  rinkeby = "rinkeby",
  kovan = "kovan",
  mumbai = "mumbai" ,
  fuji = "fuji" ,
  ganache = "ganache" ,
  localhost = "localhost",
  
}

export const contractDetail = {
  superXToken: {
    name: "SuperToken"
  },
  presale: {
    name: "PresaleERC20"
  },
  faucet: {
    name: "TestFaucet"
  },
  multicall: {
    name: "SuperXMulticall"
  }
}

