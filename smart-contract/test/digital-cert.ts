import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import { ContractTransaction } from "ethers";
import { ethers } from "hardhat";
import { print } from './helpers';
import { getUnixTime, add } from "date-fns"
import { DigitalCert__factory } from './../typechain/factories/DigitalCert__factory';
import { DigitalCert } from './../typechain/DigitalCert.d';

chai.use(chaiAsPromised);
const { expect } = chai;

describe("Digital cert.", async () => {
  let tx: ContractTransaction;
  let owner: SignerWithAddress;
  let person1: SignerWithAddress;
  let person2: SignerWithAddress;
  let deployer: SignerWithAddress;
  let minter2: SignerWithAddress;
  let minter3: SignerWithAddress;

  let contract: DigitalCert
  let DIGITAL_CERT_NAME = "DigitalCert"

  before(async () => {
    const [owner_0, person_1, person_2, person_3, person_4, person_5] = await ethers.getSigners();
    owner = owner_0;
    person1 = person_1;
    person2 = person_2;
    deployer = person_3;
    minter2 = person_4 
    minter3 = person_5
    // deploy digital cert
    const factory = await ethers.getContractFactory(DIGITAL_CERT_NAME) as DigitalCert__factory
    contract = await factory.connect(deployer).deploy(owner.address, owner.address, minter2.address, "www.superxtoken.com")
    contract = contract.connect(owner)
  })

  describe("check owner", async () => {

    it("should reject person1 is not owner", async () => {
      const adminRole = await contract.DEFAULT_ADMIN_ROLE()
      const isOwner = await contract.hasRole(adminRole, person1.address)
      expect(isOwner, "person1 is not owner").to.false
    })

    it("should fullfiled owner is contract's owner", async () => {
      const adminRole = await contract.DEFAULT_ADMIN_ROLE()
      const contractOwner = await contract.hasRole(adminRole, owner.address)
      expect(contractOwner, "owner is incorrected").to.true
    })

    it("person2 is not minter", async () => {
      const minterRole = await contract.MINTER_ROLE()
      const isMinter = await contract.hasRole(minterRole, person2.address)
      expect(isMinter, "person2 is not minter").to.false
    })

    it("owner is minter", async () => {
      const minterRole = await contract.MINTER_ROLE()
      const isMitner = await contract.hasRole(minterRole, owner.address)
      expect(isMitner, "onwer is minter").to.true
    })
  })

  describe("grant and revoke role", async () => {
   it("should grant role to person2", async () => {
    const minterRole = await contract.MINTER_ROLE()
    tx = await contract.grantRole(minterRole, person2.address)
    await tx.wait()
    const isMinter = await contract.hasRole(minterRole, person2.address)
    expect(isMinter).to.true
   })

   it("should revoke role from person2", async () => {
    const minterRole = await contract.MINTER_ROLE()
     tx = await contract.revokeRole(minterRole, person2.address)
     await tx.wait()
     const isMinter = await contract.hasRole(minterRole, person1.address)
     expect(isMinter).to.false
   })
  })

  describe("set uri by minter", async () => {
    it("should reject from set uri by person1", async () => {
      expect(contract.connect(person1).setURI("www.google.com")).to.rejected
    })

    it("should set uri by minter", async () => {
      const URI = "api.superxtoken.com"
      await contract.setURI(URI)
      const newUri = await contract.uri(1)
      expect(newUri).to.eq(URI)
    })
  })

  describe("mint new digital cert.", async () => {
    it("mint by person1 which is not minter", async () => {
      expect(contract.connect(person1).createDigitalCert(person1.address, 10, 123, 10,"0x00"), "mint by person1 should reject").to.rejected
    })

    it("create by minter", async () => {
      await contract.createDigitalCert(owner.address, 100, 123, 100,"0x00")
      const bl = await contract.balanceOf(owner.address, 1)
      expect(bl.toNumber(), "balance should = 100").to.eq(100)
      const lastId = await contract.getLastId()
      expect(lastId.toNumber(), "last id should == 1").to.eq(1)
      const cert = await contract.getDigitalCertificate(1, person1.address);
      expect(cert.expire.toNumber(), "expire should == 123").to.eq(123)
      expect(cert.price.toNumber()).to.eq(100)
    })
  })

  describe("change expire and price on existing digital certificate", async () => {
    it("should reject when change expire by person1 which is not has role minter", async () => {
      expect(contract.connect(person1).setExpireDate(1, 5849), "should reject setExpireDate by person1").to.rejected
    })

    it("should reject when change price from person1 (does not has role minter)", async () => {
      expect(contract.connect(person1).setPrice(1, 200)).to.rejected
    })

    it("should change expire date", async () => {
      const now = new Date()
      const expDate = add(now, {days: 2})
      const exp = getUnixTime(expDate)
      await contract.setExpireDate(1, exp)
      const cert = await contract.getDigitalCertificate(1, person1.address)
      expect(cert.expire.toNumber()).to.eq(exp)
    })

    it("should set new price", async () => {
      const newPrice = 200
      await contract.setPrice(1, newPrice)
      const cert = await contract.getDigitalCertificate(1, person1.address)
      expect(cert.price.toNumber()).to.eq(newPrice)
    })
  })

  describe("mint more on existing digital cert", async () => {
    it("should reject mint more which person1 not has role minter", async () => {
      expect(contract.connect(person1).mint(person1.address, 1, 900, "0x00")).to.rejected
    })

    it("should reject when mint cert that does not exites", async () => {
      expect(contract.connect(owner).mint(owner.address, 2, 100000, "0x00")).to.rejected
    })

    it("should mint more by owner", async () => {
      const addMore = 900
      await contract.connect(owner).mint(owner.address, 1, addMore, "0x00")
      const bl = await contract.balanceOf(owner.address, 1)
      expect(bl.toNumber(), "balance == 100 + 900").to.eq(1000)
    })
  })

  describe("burn", async () => {
    it("should reject when burn item does not exist", async () => {
      expect(contract.burn(owner.address, 2, 100)).to.rejected
    })

    it("should burn cert #1", async () => {
      tx = await contract.burn(owner.address, 1, 100)
      await tx.wait()
      const bl = await contract.balanceOf(owner.address, 1)
      expect(bl.toNumber(), "balance = 1000 - 100 = 900").to.eq(900)
    })
  })

  describe("mint multiple cert", async () => {
    it("should reject when provided invalid input", async () => {
      expect(contract.createDigitalCertBatch(owner.address, [100,200], [1,2,3], [10, 20], "0x00")).to.rejected
    })

    it("mint batch", async () => {
      const now = new Date()
      const exp1 = getUnixTime(add(now, {days: 2}))
      const exp2 = getUnixTime(add(now, {days: 3}))
      const expires = [exp1, exp2]
      const prices = [10,20]
      const amounts = [100, 200]
      tx = await contract.createDigitalCertBatch(owner.address, amounts, expires, prices, "0x00" )
      await tx.wait()
      const cert2 = await contract.getDigitalCertificate(2, person1.address)
      const cert3 = await contract.getDigitalCertificate(3, person1.address)

      const certs = [cert2, cert3]
      certs.forEach(async(cert, index) => {
        expect(cert.expire.toNumber(), `cert #{index + 2} expire`).to.eq(expires[index])
        expect(cert.price.toNumber(),`cert #{index + 2} price`).to.eq(prices[index])
        const bl = await contract.balanceOf(owner.address, index + 2)
        expect(bl.toNumber(), `cert #{index + 2} price`).to.eq(amounts[index])
      })
    })
  })

})