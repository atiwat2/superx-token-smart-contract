import { ethers } from "hardhat";

import chai from "chai";
import chaiAsPromised from "chai-as-promised";
chai.use(chaiAsPromised);
const { expect } = chai;

export function log(text: string, variable: any) {
  expect(console.log(text, variable));
}

export async function setBlockTimeStamp(date: Date) {
  await ethers.provider.send("evm_setNextBlockTimestamp", [
    getUnixTimeStamp(date),
  ]);
  await ethers.provider.send("evm_mine", []);
}

export async function skipBlock(day: number) {
  const timestamp = await getCurrentBlockTimeStamp();
  const dayLater = new Date(timestamp * 1000);
  dayLater.setDate(dayLater.getDate() + day);
  await ethers.provider.send("evm_setNextBlockTimestamp", [
    getUnixTimeStamp(dayLater),
  ]);
  await ethers.provider.send("evm_mine", []);
}

export async function getBlockNumber() {
  let blockNumber = await ethers.provider.getBlockNumber();
  return blockNumber;
}

export async function getCurrentBlockTimeStamp() {
  const bNo = getBlockNumber();
  const tStamp1 = (await ethers.provider.getBlock(bNo)).timestamp;
  return tStamp1;
}

export function getUnixTimeStamp(date: Date): number {
  return (date.getTime() / 1000) << 0;
}

export function print(text: string, something: any) {
	expect(console.log(text, something));
}