import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { getUnixTime, add, sub } from "date-fns"
import { ContractTransaction } from "ethers";
import { Market__factory } from './../typechain/factories/Market__factory';
import { SuperToken__factory } from './../typechain/factories/SuperToken__factory';
import { SuperToken } from './../typechain/SuperToken.d';
import { Market } from './../typechain/Market.d';
import { DigitalCert } from "../typechain/DigitalCert";
import { DigitalCert__factory } from '../typechain/factories/DigitalCert__factory';
import { print, setBlockTimeStamp } from './helpers';



chai.use(chaiAsPromised);
const { expect } = chai;

describe.only("Market", async () => {
  let tx: ContractTransaction;
  let owner: SignerWithAddress;
  let person1: SignerWithAddress;
  let person2: SignerWithAddress;
  let deployer: SignerWithAddress;
  let minter: SignerWithAddress;
  let minter2: SignerWithAddress;
  let minter3: SignerWithAddress;

  let TOKEN_NAME = "SuperToken"
  let DIGITAL_CERT_NAME = "DigitalCert"
  let MARKET_NAME = "Market"
  let tokenContract: SuperToken
  let digitalCertContract: DigitalCert
  let marketContract: Market
  let prices = [ethers.utils.parseUnits("10", 8),ethers.utils.parseUnits("20", 8)]

  before(async () => {
    const [owner_0, person_1, person_2, person_3, person_4, person_5, person_6] = await ethers.getSigners();
    owner = owner_0;
    person1 = person_1;
    person2 = person_2;
    deployer = person_3;
    minter = person_4
    minter2 = person_4 
    minter3 = person_6
    
    console.log("owner", owner.address)
    console.log("person1", person1.address)
    console.log("person2", person2.address)
    console.log("deployer", deployer.address)
    console.log("minter", minter.address)
    console.log("minter2", minter2.address)
    console.log("minter3", minter3.address)

    const tokenFactory = await ethers.getContractFactory(TOKEN_NAME) as SuperToken__factory
    tokenContract = await tokenFactory.connect(deployer).deploy("Super X Token", "SuperX", "100000000000000000", owner.address)
    tokenContract = tokenContract.connect(owner)

    const digitalCertFactory = await ethers.getContractFactory(DIGITAL_CERT_NAME) as DigitalCert__factory
    digitalCertContract = await digitalCertFactory.connect(deployer).deploy(owner.address, minter.address, minter2.address, "www.superxtoken.com")
    digitalCertContract = digitalCertContract.connect(minter)

    const marketFactory = await ethers.getContractFactory(MARKET_NAME) as Market__factory
    marketContract = await marketFactory.connect(deployer).deploy(tokenContract.address, digitalCertContract.address, owner.address, minter.address, minter2.address)
    marketContract = marketContract.connect(minter)

    const now = new Date()
    const exp1 = getUnixTime(add(now, {days: 2}))
    const exp2 = getUnixTime(add(now, {days: 1}))
    tx = await digitalCertContract.createDigitalCertBatch(marketContract.address, [100, 200], [exp1, exp2], prices, "0x00")
    await tx.wait()

    const bl1 = await digitalCertContract.balanceOf(marketContract.address, 1)
    print("bl1", bl1.toString())
    const bl2 = await digitalCertContract.balanceOf(marketContract.address, 2)
    print("bl2", bl2.toString())

    tx = await tokenContract.connect(owner).transfer(person1.address, ethers.utils.parseUnits("1000", 8))
    await tx.wait()
    const tokenBl1 = await tokenContract.balanceOf(person1.address)
    print("tokenBl1", tokenBl1.toString())
    
    tx = await tokenContract.connect(owner).transfer(person2.address, ethers.utils.parseUnits("2000", 8))
    await tx.wait()
    const tokenBl2 = await tokenContract.balanceOf(person2.address)
    print("tokenBl2", tokenBl2.toString())

    tx = await tokenContract.connect(person1).approve(marketContract.address, ethers.constants.MaxUint256)
    await tx.wait()
    tx = await tokenContract.connect(person2).approve(marketContract.address, ethers.constants.MaxUint256)
    await tx.wait()

  })

  describe("check owner and minter", async () => {
    it("should reject person1 is not owner", async () => {
      const defaultAdminRole = await marketContract.DEFAULT_ADMIN_ROLE()
      const isAdmin = await marketContract.hasRole(defaultAdminRole, person1.address)
      expect(isAdmin, "person1 is not owner").to.false
    })

    it("should be owner", async () => {
      const defaultAdminRole = await marketContract.DEFAULT_ADMIN_ROLE()
      const isAdmin = await marketContract.hasRole(defaultAdminRole, owner.address)
      expect(isAdmin).to.true
      const ownerAddress = await marketContract.ownerAddress()
      expect(ownerAddress).to.eq(owner.address)
    })

    it("should reject person1 is not minter", async () => {
      const MinterRole = await marketContract.MINTER_ROLE()
      const isMinter = await marketContract.hasRole(MinterRole, person1.address)
      expect(isMinter).to.false
    })

    it("should be minter", async () => {
      const MinterRole = await marketContract.MINTER_ROLE()
      const isMinter = await marketContract.hasRole(MinterRole, minter.address)
      expect(isMinter).to.true
    })
  })

  describe("paused feature", async () => {
    it("should reject person1 is not minter cannot pause", async () => {
      expect(marketContract.connect(person1).setPauseForCertId(1, true), "should reject person1 is not minter cannot pause").to.rejected
    })

    it("should pause by minter", async () => {
      tx = await marketContract.connect(minter).setPauseForCertId(1, true)
      await tx.wait()
      const isCert1Paused = await marketContract.connect(minter).isDigitalCertPaused(1)
      expect(isCert1Paused, "cert1 should paused").to.true
    })

    it("should reject unpause by person1", async () => {
      expect(marketContract.connect(person1).setPauseForCertId(1, false), "should reject unpause by person1").to.rejected
    })

    it("should unpaused by minter", async () => {
      tx = await marketContract.setPauseForCertId(1, false)
      await tx.wait()
      const isCert1Paused = await marketContract.connect(minter).isDigitalCertPaused(1)
      expect(isCert1Paused).to.false
    })
  })

  describe("redeem", async () => {
    it("should reject when redeem paused item", async () => {
      tx = await marketContract.connect(minter).setPauseForCertId(1, true)
      await tx.wait()
      expect(marketContract.connect(person1).onRedeem(1, 10), "reject redeem cert #1 is paused").to.rejected
      // reset for next it()
      tx = await marketContract.connect(minter).setPauseForCertId(1, false)
      await tx.wait()
    })

    it("should reject when redeem exeed available digital cert", async () => {
      expect(marketContract.connect(person1).onRedeem(1, 900)).to.rejected
    })

    it("should reject when redeem on expired cert", async () => {
      const now = new Date()
      const tenDay = add(now, {days:1})
      await setBlockTimeStamp(tenDay)
      expect(marketContract.connect(person1).onRedeem(2, 10), "redeem on expired").to.rejected
    })

    it("should reject when redeem with no any token from deployer", async () => {
      expect(marketContract.connect(deployer).onRedeem(1,10)).to.rejected
    })

    it("should redeem from person1", async () => {
      const redeemAmount = 10
      const balanceBeforeRedeem = await tokenContract.balanceOf(person1.address)
      const certBlBefore = await digitalCertContract.balanceOf(marketContract.address, 1)
      // redeem
      tx = await marketContract.connect(person1).onRedeem(1, redeemAmount)
      await tx.wait()
      print("hash", tx.hash)
      const redeemItem = await marketContract.getRedeemByRedeemId(1)
      expect(redeemItem.certId, "cert id == 1").to.eq(1)
      expect(redeemItem.redeemedId, "redeem id == 1").to.eq(1)
      expect(redeemItem.redeemer, "redeemer == person1").to.eq(person1.address)
      expect(redeemItem.amount, "amount == 10").to.eq(10)
      
      const cost = ethers.BigNumber.from(10).mul(prices[0])
      const balanceAfterRedeem = await tokenContract.balanceOf(person1.address)
      expect(balanceAfterRedeem.toString(), "balance after redeem").to.eq(balanceBeforeRedeem.sub(cost).toString())
      
      const certBlAfter = await digitalCertContract.balanceOf(marketContract.address, 1)
      expect(certBlBefore.sub(redeemAmount), "remaining cert on market").to.eq(certBlAfter)
      
      const lastRedeemId = await marketContract.getLastRedeemId()
      expect(lastRedeemId.toNumber(), "last redeem id").to.eq(1)

      const redeemIdsByAddress = await marketContract.getRedeemIdsByAddress(person1.address)
      const redeemIdsByAddressNum = redeemIdsByAddress.map(id => id.toNumber())
      expect(redeemIdsByAddressNum, "redeem ids by address").to.have.deep.eq([1])

      const redeemData = await marketContract.getRedeemByRedeemId(redeemIdsByAddress[0])
      expect(redeemData.redeemedId, "redeem id").to.eq(redeemIdsByAddress[0])
      expect(redeemData.redeemer, "redeemer").to.eq(person1.address)
      expect(redeemData.certId.toNumber(), "cert id").to.eq(1)
      expect(redeemData.amount.toNumber(), "amount").to.eq(10)
    })
  })

  describe("burn", async () => {
    it("should not burn from person1", async () => {
      expect(marketContract.connect(person1).burnFor(2, 10)).to.rejected
    })

    it("should not burnBatch from person1", async () => {
      expect(marketContract.connect(person1).burnBatchFor([1,2], [10,20])).to.rejected
    })

    it("should burn from minter", async () => {
      const id2Bl = await digitalCertContract.balanceOf(marketContract.address, 2)
      const id2burnAmount = 20
      const tx = await marketContract.connect(minter).burnFor(2, id2burnAmount)
      await tx.wait()
      const id2BlAfter =  await digitalCertContract.balanceOf(marketContract.address, 2)
      expect(id2BlAfter.add(id2burnAmount), "after burn 20 token should equal").to.eq(id2Bl)
    })
    
    it("should burn batch from minter", async () => {
      const id1Bl = await digitalCertContract.balanceOf(marketContract.address, 1)
      const id2Bl = await digitalCertContract.balanceOf(marketContract.address, 2)
      const id1Burn = 10
      const id2Burn = 20
      tx = await marketContract.connect(minter).burnBatchFor([1,2], [id1Burn, id2Burn])
      await tx.wait()
      const id1BlAfter = await digitalCertContract.balanceOf(marketContract.address, 1)
      const id2BlAfter = await digitalCertContract.balanceOf(marketContract.address, 2)
      expect(id1BlAfter.add(id1Burn), "ID == 1 burn 10").to.eq(id1Bl)
      expect(id2BlAfter.add(id2Burn), "id == 2 burn 20").to.eq(id2Bl)
    })
  })

})