import { PresaleERC20__factory } from './../typechain/factories/PresaleERC20__factory';
import { PresaleERC20 } from './../typechain/PresaleERC20.d';
import { contractDetail } from '../constant';
import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ContractTransaction } from "ethers";
import { ethers } from "hardhat";
import { SuperToken } from '../typechain/SuperToken';
import { SuperToken__factory } from '../typechain/factories/SuperToken__factory';
import { env } from '../config/config';



chai.use(chaiAsPromised);
const { expect } = chai;

const TOKEN_NAME = "SuperToken"

describe("testing Presale", async () => {
  let CSuperToken: SuperToken;
  let CPresale: PresaleERC20
  let tx: ContractTransaction;
  let owner: SignerWithAddress;
  let person1: SignerWithAddress;
  let person2: SignerWithAddress;
  let deployer: SignerWithAddress;
  
  before(async () => {
    const [owner_0, person_1, person_2, person_3] = await ethers.getSigners();
    owner = owner_0;
    person1 = person_1;
    person2 = person_2;
    deployer = person_3;


    // deploy SuperX
    const tokenFactory = await ethers.getContractFactory(TOKEN_NAME) as SuperToken__factory
    CSuperToken = await tokenFactory.connect(deployer).deploy("Super X Token", "SuperX", "100000000000000000", owner.address)
    await CSuperToken.deployed();
    CSuperToken = CSuperToken.connect(owner)

    // deploy preslae
    const FPresale = await ethers.getContractFactory(contractDetail.presale.name, deployer) as PresaleERC20__factory
    CPresale = await FPresale.deploy(CSuperToken.address, person1.address, owner.address)
    await CPresale.deployed()
  })

  it("should show Role == DEFAULT_ADMIN_ROLE", async () => {
    const adminRoleBytes = await CPresale.DEFAULT_ADMIN_ROLE()
    // print("adminRoleBytes", adminRoleBytes)
    const hasRole = await CPresale.hasRole(adminRoleBytes, owner.address)
    // print("hasRole", hasRole)
    expect(hasRole).to.true
   
  })

  it("should show Role == TRANSFER_ROLE", async () => {
    const transferRoleBytes = await CPresale.TRANSFER_ROLE()
    // print("transferRoleBytes",transferRoleBytes)
    const transferRole = await CPresale.hasRole(transferRoleBytes, person1.address)
    expect(transferRole).to.true
  })


  context("transfer SuperX token", async () => {
    before(async () => {
      tx = await CSuperToken.transfer(CPresale.address, 1000 * 1e8)
      await tx.wait()
    })

    it("should transfer batch", async () => {
      tx = await CPresale.connect(person1).transferBatch([person2.address, deployer.address], [10 * 1e8, 20 * 1e8])
      await tx.wait()

      const bl1 = await CSuperToken.balanceOf(person2.address)
      expect(bl1.toNumber()).to.eq(10 * 1e8)
      const bl2 = await CSuperToken.balanceOf(deployer.address)
      expect(bl2.toNumber()).to.eq(20 * 1e8)
    })

    it("should failed due call by other guy are not has role TRANSFER_ROLE including owner as well", async () => {
      expect(CPresale.connect(deployer).transferBatch([person2.address, deployer.address], [10 * 1e8, 20 * 1e8])).to.rejected
      expect(CPresale.connect(owner).transferBatch([person2.address, deployer.address], [10 * 1e8, 20 * 1e8])).to.rejected
    })

  })
})