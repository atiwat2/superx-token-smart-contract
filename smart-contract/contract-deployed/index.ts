import deployedJson from "./deployed.json"
import { IAddresses, NETWORKS, TAddresses, TNetwork } from "./contract-deployed.interface";

const deployedData: {[chainId: number]: IAddresses[]} = deployedJson

export function getDeployedMap() {
  const map = new Map<keyof typeof NETWORKS, IAddresses[]>();
  for (const [key, value] of Object.entries(deployedData)) {
    map.set(Number(key), value)
  }
  return map
}

export function formatMapToObject(m: Map<keyof typeof NETWORKS, IAddresses[]>) {
  const deployedAddressObj: {[chainId: number]: IAddresses[]} = { }
  m.forEach((value, key) => {
    deployedAddressObj[key] = value
  })
  return deployedAddressObj
}

export function getContractAddressByContractName(networkId: TNetwork, contract: TAddresses) {
  if (contract == "BlockNumber") return null
  const deployed = getDeployedMap()
  const allAddressesInNetwork = deployed.get(networkId)
  if (!allAddressesInNetwork) return null
  if (allAddressesInNetwork.length <= 0) return null
  const address = allAddressesInNetwork[0][contract]
  if (!address || address == "") return null
  return address
}

export function getBlockNumber(networkId: TNetwork) {
  const deployed = getDeployedMap()
  const allAddressesInNetwork = deployed.get(networkId)
  if (!allAddressesInNetwork) return null
  if (allAddressesInNetwork.length <= 0) return null
  const blockNumber = allAddressesInNetwork[0]["BlockNumber"]
  if (!blockNumber || blockNumber <= 0) return null
  return blockNumber
}

export function getAllContractAddressForNetwork(networkId: TNetwork) {
  const deployed = getDeployedMap()
  const allAddressesInNetwork = deployed.get(networkId)
  return allAddressesInNetwork
}