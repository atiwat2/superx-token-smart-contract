export const NETWORKS: {[chainId: number]: string} = {
  1: "Ethereum Mainnet",
  3: "Ropsten Test Network",
  4: "Rinkeby Test Network",
  5: "Goerli Test Network",
  42: "Kovan Test Network",
  1337: "Localhost 7545",
  43113: "Fuji Test Network",
  80001: "Mumbai Polygon Test Network"
  // 1337: "ganache",
}


export interface IAddresses {
  BlockNumber: number
  Token: string
  DigitalCert: string
  Market: string
  Multicall: string
  Presale: string
}

export type TAddresses = keyof IAddresses
export type TNetwork = keyof typeof NETWORKS