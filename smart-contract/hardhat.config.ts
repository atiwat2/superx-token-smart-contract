import { config as dotEnvConfig } from "dotenv";
dotEnvConfig();

import { HardhatUserConfig } from "hardhat/types";

import "@nomiclabs/hardhat-waffle";
import "@typechain/hardhat";
import "@nomiclabs/hardhat-etherscan";
import "hardhat-interface-generator";
import "solidity-coverage";
import { env } from "./config/config";
import { ENetworkName } from "./constant";

const INFURA_API_KEY = process.env.INFURA_API_KEY || "";

const config: HardhatUserConfig = {
  defaultNetwork: "hardhat",
  solidity: {
    compilers: [{ version: "0.8.0", settings: {} }],
  },
  paths: {
    // root: string;
    // configFile: string;
    // cache: string;
    // artifacts: string;
    // sources: string;
    // tests: string;
  },
  
  networks: {
    hardhat: {
      mining: {
        auto: true,
        // interval: 5000,
      },
      chainId: 1337,
      accounts: {
        mnemonic: env.mnemonic,
        count: 20,
        accountsBalance: "100000000000000000000000"
      },
      loggingEnabled: false,
      // allowUnlimitedContractSize: true,
      // blockGasLimit: 0x1fffffffffffff
    },
    [ENetworkName.localhost]: {
      url: "http://127.0.0.1:8545/",
      accounts: {
        mnemonic: env.mnemonic,
        count: 20
      }
    },
    [ENetworkName.ganache]: {
      url: "http://127.0.0.1:7545",
      accounts: {
        mnemonic: env.mnemonic,
        count: 20
      },
      chainId: 1337
    },
    [ENetworkName.rinkeby]: {
      url: env[4].providerUrl,
      accounts: [env.deployerPrivateKey],
      chainId: 4
    },
    coverage: {
      url: "http://127.0.0.1:8555", // Coverage launches its own ganache-cli client
    },
    [ENetworkName.fuji]: {
      url: "https://api.avax-test.network/ext/bc/C/rpc",
      gasPrice: 225000000000,
      chainId: 43113,
      accounts: [ env.deployerPrivateKey ],
    },
    [ENetworkName.mumbai]: {
      url: "https://rpc-mumbai.matic.today",
      chainId: 80001,
      accounts: [env.deployerPrivateKey],
    },
  },
  etherscan: {
    // Your API key for Etherscan
    // Obtain one at https://etherscan.io/
    apiKey: env[1].blockScanKey,
  },
};

export default config;
