import fs from 'fs/promises'
import path from 'path'
import { ENetworkName } from '../constant'
import { IDeployedContract } from './helpers/deployed'

const currenctDir = path.join(process.cwd(), "/deployed.json")

async function isExists(path:string) {
  try {
    await fs.access(path)
    return true
  } catch {
    return false
  }
}

async function getOldJson() {
  let deployedTemplate: IDeployedContract = {
    main: [],
    kovan: [],
    rinkeby: [],
    mumbai: [],
    fuji: [],
    ganache: [],
    localhost: [],
  }

  let deployedStr = ""
  const exist = await isExists(currenctDir)
  if (exist) {
    const b = await fs.readFile(currenctDir).catch()
    deployedStr = b.toString()
  }

  if (deployedStr !== "") {
    const deployed: IDeployedContract = JSON.parse(deployedStr)
    console.log( deployed)
    deployedTemplate = {
      ...deployedTemplate,
      ...deployed
    }    
  }

  return deployedTemplate
}

export async function writeContractAddress(networkName:ENetworkName,startBlock:number ,token:string, future: string, priceFeed:string, exchange:string,presale: string, multicall:string,testFaucet:string = "") {
  
  const deployedDatas = await getOldJson()
  const deployedData = deployedDatas[networkName]



  const data = {
    startBlock: startBlock,
    token,
    future,
    priceFeed,
    exchange,
    presale,
    multicall,
    testFaucet
  }

  deployedDatas[networkName] = [data, ...deployedData]

  try {
    return await fs.writeFile(currenctDir, JSON.stringify(deployedDatas))
  } catch (error) {
    console.error("error while write contract address", error)
    return
  }

  
}

