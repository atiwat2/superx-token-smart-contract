import { PresaleERC20 } from './../typechain/PresaleERC20.d';
import { SuperXMulticall } from './../typechain/SuperXMulticall.d';
import { Market__factory } from './../typechain/factories/Market__factory';
import { Market } from './../typechain/Market.d';
import { DigitalCert__factory } from './../typechain/factories/DigitalCert__factory';
import { DigitalCert } from './../typechain/DigitalCert.d';

import { SuperXMulticall__factory } from "./../typechain/factories/SuperXMulticall__factory";
import { PresaleERC20__factory } from "./../typechain/factories/PresaleERC20__factory";
import { contractDetail } from "./../constant";
import { TestFaucet__factory } from "./../typechain/factories/TestFaucet__factory";

import { SuperToken__factory } from "./../typechain/factories/SuperToken__factory";
import { ethers, network } from "hardhat";
import { BigNumber, ContractTransaction, Signer } from "ethers";
import { env } from "../config/config";

import { SuperToken } from "../typechain/SuperToken";

import { getNetworkName } from "./helpers/deployed";
import { HttpNetworkConfig } from "hardhat/types";
import { IAddresses, TAddresses } from "../contract-deployed/contract-deployed.interface";
import { getBlockNumber, getContractAddressByContractName } from '../contract-deployed';
import { JsonRpcProvider } from '@ethersproject/providers';
import { writeDeployedToJson } from './writeContractAddressToTS';

const USE_OLD_DEPLOYED_ADDRESS = new Map<TAddresses, boolean>()
USE_OLD_DEPLOYED_ADDRESS.set("BlockNumber", false)
USE_OLD_DEPLOYED_ADDRESS.set("Token", false)
USE_OLD_DEPLOYED_ADDRESS.set("Presale", false)
USE_OLD_DEPLOYED_ADDRESS.set("DigitalCert", false)
USE_OLD_DEPLOYED_ADDRESS.set("Market", false) // depends on token and digitalCert contract 
USE_OLD_DEPLOYED_ADDRESS.set("Multicall", false) // depends on digitalCert and Market

const SuperTokenName = contractDetail.superXToken.name;
const DigitalCertName = "DigitalCert"
const MarketName = "Market"
const FaucetName = contractDetail.faucet.name;
const Multicallname = contractDetail.multicall.name;
const PresaleName = "PresaleERC20"

let tx: ContractTransaction;

async function deploySuperXToken(ownerPublicKey: string): Promise<SuperToken> {
  console.log("deploying SuperX token...");
  // deploy ERC20
  const FToken = (await ethers.getContractFactory(
    SuperTokenName
  )) as SuperToken__factory;
  const CToken = await FToken.deploy(
    env.token.name,
    env.token.symbol,
    env.token.mintAmount,
    ownerPublicKey
  );
  await CToken.deployed();
  return CToken;
}
async function getOldSuperXToken(chainId:number): Promise<SuperToken> {
  const factory = await ethers.getContractFactory(SuperTokenName) as SuperToken__factory
  const address = getContractAddressByContractName(chainId, "Token")
  if (!address) throw new Error("Not found address of SuperXToken Contract")
  const contract = factory.attach(address as string)
  return contract
}

async function deployDigitalCert(ownerPublicKey:string, minterPublicKey:string, minter2PublicKey:string, uri: string): Promise<DigitalCert> {
  console.log("deploying DigitalCert...")
  const factory = await ethers.getContractFactory(DigitalCertName) as DigitalCert__factory
  const contract = await factory.deploy( ownerPublicKey,minterPublicKey,minter2PublicKey, uri)
  await contract.deployed()
  return contract
}

async function getOldDigitalCert(chainId:number): Promise<DigitalCert> {
  const facotry = await ethers.getContractFactory(DigitalCertName) as DigitalCert__factory
  const address = getContractAddressByContractName(chainId, "DigitalCert")
  if (!address) throw new Error("Not found DigitalCert Contract Address")
  const contract = facotry.attach(address as string)
  return contract
}

async function deployMarket(tokenAddress: string, digitalCertAddress:string, ownerAddress:string, minterAddress:string, minter2Address:string ): Promise<Market> {
  console.log("deploying Market...")
  const factory = await ethers.getContractFactory(MarketName) as Market__factory
  const contract = await factory.deploy(tokenAddress, digitalCertAddress, ownerAddress, minterAddress, minter2Address)
  await contract.deployed()
  return contract
}

async function getOldMarket(chainId:number): Promise<Market> {
  const factory = await ethers.getContractFactory(MarketName) as Market__factory
  const address = getContractAddressByContractName(chainId, "Market")
  if (!address) throw new Error("Not found Market Contract address")
  const contract = factory.attach(address as string)
  return contract
}

async function deployPresale(
  CTokenAddress: string,
  ownerPublicKey: string,
  transfererPublicKey: string
) {
  console.log("deploying Presale...");
  const FPresale = (await ethers.getContractFactory(
    contractDetail.presale.name
  )) as PresaleERC20__factory;
  const CPresale = await FPresale.deploy(
    CTokenAddress,
    transfererPublicKey,
    ownerPublicKey
  );
  await CPresale.deployed();
  return CPresale;
}

async function getOldPresale(chainId:number): Promise<PresaleERC20> {
  const address = getContractAddressByContractName(chainId, "Presale")
  if (!address) throw new Error("not found presale cotnract address")
  const factory = await ethers.getContractFactory(PresaleName) as PresaleERC20__factory
  const contract = factory.attach(address as string)
  return contract
}

async function deployMulticall(
  CDigitalCert: string,
  CMarket: string,
) {
  console.log("deploying multicall...");
  const FMulticall = (await ethers.getContractFactory( Multicallname )) as SuperXMulticall__factory;
  const CMulticall = await FMulticall.deploy(CDigitalCert, CMarket)
  await CMulticall.deployed();
  return CMulticall;
}

async function getOldMulticall(chainId:number): Promise<SuperXMulticall> {
  const address = getContractAddressByContractName(chainId, "Multicall")
  if (!address) throw new Error("Not found Multicall Contract Address")
  const factory = await ethers.getContractFactory(Multicallname) as SuperXMulticall__factory
  const contract = factory.attach(address as string)
  return contract
}

async function deployTestFaucet(CTokenAddress: string) {
  console.log("deploying Faucet...");
  const FFaucet = (await ethers.getContractFactory(
    FaucetName
  )) as TestFaucet__factory;
  const CFaucet = await FFaucet.deploy(CTokenAddress);
  await CFaucet.deployed();
  return CFaucet;
}

async function getDeployerBalance(provider: JsonRpcProvider): Promise<BigNumber> {
  const signer = provider.getSigner(env.deployerPublicKey)
  return await signer.getBalance()
}

async function logGasUsed(text:string, startBalance: BigNumber, finalBalance:BigNumber) {
  const used = startBalance.sub(finalBalance)
  console.log(text)
  console.table({startBalance: startBalance.toString(), finalBalance: finalBalance.toString(), used: used.toString()})
  console.table({startBalance: ethers.utils.formatEther(startBalance), finalBalance: ethers.utils.formatEther(finalBalance), used: ethers.utils.formatEther(used)})
}

async function main() {
  // network
  console.dir(network);
  const chainId = network.config.chainId
  if (!chainId) throw new Error("ChainId is invalid")

  const ownerPublicKey = env.ownerPublicKey
  const minterPublicKey = env.minterPublicKey
  const minter2PublicKey = env.minter2PublicKey
  const transfererPublicKey = env.transfererPublicKey
  const networkName = getNetworkName();
  // const provider = ethers.getDefaultProvider()
  // console.log("provider", provider.network)
  // const blockNumber = await provider.getBlockNumber()
  // const newProvider = new ethers.providers.JsonRpcProvider(network.config.url)
  const provider = new ethers.providers.JsonRpcProvider(
    (network.config as HttpNetworkConfig).url
  );
  let blockNumber = await provider.getBlockNumber();

  console.table({ workingAt: blockNumber });
  console.table({ networkName: networkName, ownerPublicKey });

  if (USE_OLD_DEPLOYED_ADDRESS.get("BlockNumber") == true) {
    const blockNum = getBlockNumber(chainId)
    if (!blockNum) {
      console.log("USE_OLD_DEPLOYED_ADDRESS -> BlockNumber == true | but there is no block number")
      return 
    }
    blockNumber = blockNum
  }

  const startBalance = await getDeployerBalance(provider)
  console.log("🚀 ~ file: deploy.ts ~ line 183 ~ main ~ startBalance", startBalance)

  // deploy ERC20
  let CToken: SuperToken
  if (!USE_OLD_DEPLOYED_ADDRESS.get("Token")) {
    const startBl = await getDeployerBalance(provider)
    CToken = await deploySuperXToken(ownerPublicKey)
    console.table({ "Super Token Contract address": CToken.address });
    const remainBl = await getDeployerBalance(provider)
    logGasUsed("--- Gas deployed SuperX Token ---", startBl, remainBl)
  }else{
   CToken = await getOldSuperXToken(chainId)
  }
  
  // deploy digital cert
  let CDigitalCert: DigitalCert
  if (!USE_OLD_DEPLOYED_ADDRESS.get("DigitalCert")) {
    const startBl = await getDeployerBalance(provider)
    CDigitalCert = await deployDigitalCert(ownerPublicKey, minterPublicKey, minter2PublicKey, env.digitalCertUri)
    console.table({ "DigitalCert Contract address": CDigitalCert.address });
    const remainBl = await getDeployerBalance(provider)
    logGasUsed("--- Gas deployed DigitalCert ---", startBl, remainBl)
  }else{
    CDigitalCert = await getOldDigitalCert(chainId)
  }

  // deploy market
  let CMarket: Market
  if (!USE_OLD_DEPLOYED_ADDRESS.get("Market")) {
    const startBl = await getDeployerBalance(provider)
    CMarket = await deployMarket(CToken.address, CDigitalCert.address, ownerPublicKey, minterPublicKey, minter2PublicKey)
    console.table({ "Market Contract address": CMarket.address });
    const remainBl = await getDeployerBalance(provider)
    logGasUsed("--- Gas deployed Market ---", startBl, remainBl)
  }else{
    CMarket = await getOldMarket(chainId)
  }

  // deploy multicall
  let CMulticall: SuperXMulticall
  if (!USE_OLD_DEPLOYED_ADDRESS.get("Multicall")) {
    const startBl = await getDeployerBalance(provider)
    CMulticall = await deployMulticall(CDigitalCert.address, CMarket.address)
    console.table({ "Multicall Contract address": CMulticall.address });
    const remainBl = await getDeployerBalance(provider)
    logGasUsed("--- Gas deployed Multicall ---", startBl, remainBl)
  }else{
    CMulticall = await getOldMulticall(chainId)
  }

  // deploy presale
  let CPresale: PresaleERC20
  if (!USE_OLD_DEPLOYED_ADDRESS.get("Presale")) {
    const startBl = await getDeployerBalance(provider)
    CPresale = await deployPresale(CToken.address, ownerPublicKey, transfererPublicKey)
    console.table({ "Presale Contract address": CPresale.address });
    const remainBl = await getDeployerBalance(provider)
    logGasUsed("--- Gas deployed Presale ---", startBl, remainBl)
  }else{
    CPresale = await getOldPresale(chainId)
  }

  const finalBalance = await getDeployerBalance(provider)
  logGasUsed("Total Gas Used", startBalance, finalBalance)

  const addresses: IAddresses = {
    BlockNumber: blockNumber,
    Token: CToken.address,
    DigitalCert: CDigitalCert.address,
    Market: CMarket.address,
    Multicall: CMulticall.address,
    Presale: CPresale.address
  }

  await writeDeployedToJson(chainId, addresses)
  // end main
}

const runMain = async () => {
  try {
    await main();
    process.exit(0);
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

runMain();
