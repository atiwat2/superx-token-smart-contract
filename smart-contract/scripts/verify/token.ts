import { env } from "../../config/config";
import { getDeployedContractAddress } from "../helpers/deployed";

export default [
  env.Token.name,
  env.Token.symbol,
  env.TokenMintAmount.toString(),
  env.rinkeby.OwnerPublicKey,
];
