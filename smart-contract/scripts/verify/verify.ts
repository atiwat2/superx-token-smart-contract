import { run, network } from "hardhat";
import { env } from "../../config/config";

import { getContractAddressByContractName } from "../../contract-deployed/index"

// verify token
async function verifyToken() {
  console.log("verifying token");
  return await run("verify:verify", {
    address: getContractAddressByContractName(4, "Token"),
    constructorArguments: [
      env.token.name,
      env.token.symbol,
      env.token.mintAmount,
      env.ownerPublicKey,
    ],
  });
}

async function verifyDigitalCert() {
  console.log("verifying digital cert...")
  return await run("verify:verify", {
    address: getContractAddressByContractName(4, "DigitalCert"),
  
    constructorArguments: [
      env.ownerPublicKey,
      env.minterPublicKey,
      env.minter2PublicKey,
      env.digitalCertUri
    ]
  })
}

async function verifyMulticall() {
  console.log("verifying multi call")
  return await run("verify:verify", {
    address: getContractAddressByContractName(4, "Multicall"),
    constructorArguments: [
      getContractAddressByContractName(4, "DigitalCert"),
      getContractAddressByContractName(4, "Market")
    ]
  })
}

async function verifyMarket() {
  console.log("verifying market")
  return await run("verify:verify", {
    address: getContractAddressByContractName(4, "Market"),
    constructorArguments: [
      getContractAddressByContractName(4, "Token"),
      getContractAddressByContractName(4, "DigitalCert"),
      env.ownerPublicKey,
      env.minterPublicKey,
      env.minter2PublicKey
    ]
  })
}

// verify presale
async function verifyPresale() {
  console.log("verify presale")
  return await run("verify:verify", {
    address: getContractAddressByContractName(4, "Presale"),
    constructorArguments: [
      getContractAddressByContractName(4, "Token"),
      env.transfererPublicKey,
      env.ownerPublicKey
    ]
  })
}

async function main() {
  // await verifyToken().catch((error) => {
  //   console.log("verify token error: ", error);
  // });
  // await verifyDigitalCert().catch(error => {
  //   console.log("verify digital cert error:", error)
  // })
  await verifyMarket().catch(error => {
    console.log("verify market error", error)
  })
  await verifyMulticall().catch(error => {
    console.log("verify multicall error:", error)
  })
  // await verifyPresale().catch(error => {
  //   console.log("verify price feed error: ", error)
  // })
}

main();
