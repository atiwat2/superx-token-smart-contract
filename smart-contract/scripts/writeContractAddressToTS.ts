import fs from 'fs/promises';


import path from 'path';
import { formatMapToObject, getAllContractAddressForNetwork, getDeployedMap } from "../contract-deployed"
import { IAddresses, TNetwork } from '../contract-deployed/contract-deployed.interface';

const DIR = path.join(process.cwd(), "/contract-deployed/deployed.json")

const dummy: IAddresses =  {
  BlockNumber: 0,
  Token: "token2",
  DigitalCert: "cert2",
  Market: "market2",
  Multicall: "multicall2",
  Presale: "presale2"
}

export async function writeDeployedToJson(networkId: TNetwork, address: IAddresses) {
  let allContractAddresses: IAddresses[] = []
  const contractAddresses = getAllContractAddressForNetwork(networkId)
  if (!contractAddresses) {
    allContractAddresses = [address]
  }else{
    allContractAddresses = [address, ...contractAddresses]
  }

  const all = getDeployedMap()
  all.set(networkId, allContractAddresses)
  const obj = formatMapToObject(all)
  console.log("🚀 ~ file: writeContractAddressToTS.ts ~ line 31 ~ writeDeployedToJson ~ obj", obj)
  
  try {
    return await fs.writeFile(DIR, JSON.stringify(obj))
  } catch (error) {
    console.error("error while write contract address", error)
    return
  } 
}