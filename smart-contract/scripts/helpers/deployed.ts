import { ENetworkName } from './../../constant';
import { network } from "hardhat";

export function getNetworkName(): ENetworkName {
  switch (network.name) {
    case "main": return ENetworkName.main
    case "rinkeby": return ENetworkName.rinkeby
    case "kovan": return ENetworkName.kovan
    case "mumbai": return ENetworkName.mumbai
    case "fuji": return ENetworkName.fuji
    case "ganache": return ENetworkName.ganache
    case "localhost": return ENetworkName.localhost
    default: return ENetworkName.localhost
  }
}


