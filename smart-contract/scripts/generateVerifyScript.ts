import fs from "fs/promises";
import path from "path";
import { getDeployedContractAddress } from "./helpers/deployed";

const verificationFilePaths = {
  token: "scripts/verify/token.ts",
  priceFeed: "scripts/verify/pricefeed.ts",
  future: "scripts/verify/future.ts",
  exchange: "scripts/verify/exchange.ts",
};

const deployed = getDeployedContractAddress()

const data = {
  token: getVerifyScriptTextForContract(
    deployed!.token,
    verificationFilePaths.token
  ),
  priceFeed: getVerifyScriptTextForContract(
    deployed!.priceFeed,
    verificationFilePaths.priceFeed
  ),
  future: getVerifyScriptTextForContract(
    deployed!.future,
    verificationFilePaths.future
  ),
  exchange: getVerifyScriptTextForContract(
    deployed!.exchange,
    verificationFilePaths.exchange
  ),
};

async function generateVerifyScriptText() {
  const verifyDir = path.join(process.cwd(), "/verify.json");
  try {
    await fs.writeFile(verifyDir, JSON.stringify(data));
  } catch (error) {
    return;
  }
}

async function generateVerifyMakefile() {
  const text = `
verify-token:
${String.fromCharCode(9)}${data.token}
verify-price-feed:
${String.fromCharCode(9)}${data.priceFeed}
verify-future:
${String.fromCharCode(9)}${data.future}
verify-exchange:
${String.fromCharCode(9)}${data.exchange}
all:
${String.fromCharCode(9)}make verify-token
${String.fromCharCode(9)}make verify-price-feed
${String.fromCharCode(9)}make verify-future
${String.fromCharCode(9)}make verify-exchange
  `;
  const verifyDir = path.join(process.cwd(), "/Makefile");
  try {
    await fs.writeFile(verifyDir, text);
  } catch (error) {
    return;
  }
}

function getVerifyScriptTextForContract(
  contractAddress: string,
  verificationFilePath: string
) {
  return `npx hardhat verify --network rinkeby ----constructor-args ${verificationFilePath} ${contractAddress}`;
}

// generateVerifyScriptText()
generateVerifyMakefile();
