import { BigNumber } from 'ethers';
import yamlToEnv from 'yaml-to-env'

yamlToEnv({
    yamlPath: 'config/config.yml',
    exposeVariables: [
        "MAINNET_PROVIDER_URL",
        "RINKEBY_PROVIDER_URL",
        "ETHERSCAN_KEY",
        "POLYGONSCAN_KEY",
        "DEPLOYER_PRIVATE_KEY",
        "MNEMONIC"
    ]
})

interface IChainData {
    blockScanKey?:string
    providerUrl: string
}

interface IENV {
    [chainId: number]: IChainData
    ownerPublicKey: string
    transfererPublicKey: string
    minterPublicKey: string
    minter2PublicKey:string
    deployerPrivateKey: string
    deployerPublicKey:string
    mnemonic: string
    token: {
        name: string
        symbol:string
        mintAmount: BigNumber
    }
    digitalCertUri: string
}

export const env: IENV = {
    1: {
        blockScanKey: process.env.ETHERSCAN_KEY || "",
        providerUrl: process.env.MAINNET_PROVIDER_URL || "",
    },
    4: {
        blockScanKey: process.env.ETHERSCAN_KEY,
        providerUrl: process.env.RINKEBY_PROVIDER_URL || "",
    },
    1337: {
        blockScanKey: "",
        providerUrl: "http://localhost:7545"
    },
    deployerPrivateKey: process.env.DEPLOYER_PRIVATE_KEY || "",
    deployerPublicKey: "0x95C9CE2CF8A726017Ba62A7785b5575DD06DD899",
    ownerPublicKey: "0x9B9Ff2d45Be72d122E8CEabcBE086C7b187D593A",
    transfererPublicKey: "0xfF2Ed6d6D92efD9E27dC00501b2dCf66CF7c32aD",
    minterPublicKey: "0xAB5F0CeC1A001B069e15afd05A470d7Cdf9AdCc7",
    minter2PublicKey:"0x55De93aA70Ecc1a71F66BC3F6f243Ab8f23A0A7C",
    mnemonic: process.env.MNEMONIC || "",
    token: {
        name: "Super X Token",
        symbol: "SUPERX",
        mintAmount: BigNumber.from(1e3).mul(1e6).mul(1e8)
    },
    digitalCertUri: "http://localhost:8000/api/metadata/{id}"
    // digitalCertUri: "http://api-dev.superxtoken.com/api/metadata/{id}"
}