// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract SuperToken is ERC20, Ownable {
    constructor(string memory _name, string memory _symbol, uint256 _mintAmount,address _owner) ERC20(_name, _symbol) {
        _mint(_owner, _mintAmount);
        transferOwnership(_owner);
    }

    function decimals() public view virtual override returns (uint8) {
        return 8;
    }
}