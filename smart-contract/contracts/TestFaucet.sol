// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

/// @title A this contract will use for TEST only in production will not deploy
/// @Certik this contract you do not need to inspect
import "contracts/SuperToken.sol";

/// @title A Faucet contract for testing only give SuperX token to who need
/// @author Super X
/// @notice Give user a SuperX token pass your public key address to function
/// @dev should transfer SuperX token into this contract then transfer to user. This contract does not minting any SuperTokenX.
contract TestFaucet {
  SuperToken superX;
  constructor(address _superXAddress) {
    superX = SuperToken(_superXAddress);
  }

/// @notice give SuperX Token to user 
/// @dev none
/// @param _user a user public key address
/// @param _amount amount of SuperX token that user want
  function getSuperX(address _user,uint256 _amount) public {
    uint256 max = 10**8 * 100000;
    require(_amount <= max, "maximum 100000 SuperX");
    superX.transfer(_user, _amount);
  }
  

}